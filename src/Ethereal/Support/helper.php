<?php
// helper - global functions

if (!function_exists('app')) {
    function app()
    {
        return;
    }
}

if (!function_exists('env')) {
    function env(string $key, $fallback = null)
    {
        return $_ENV[$key] ?? getenv($key) ?? $fallback;
    }
}

if (!function_exists('config')) {
    function config()
    {
        return;
    }
}
