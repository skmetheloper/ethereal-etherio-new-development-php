<?php
namespace Ethereal\Foundaiton;

use Ethereal\Container\Container;

class Application extends Container
{
    protected $basePath;

    protected function setBasePath($basePath)
    {
        return $this->basePath = $basePath;
    }
}
