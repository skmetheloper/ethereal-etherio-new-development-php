<?php

if (env('APP_ENV') === 'dev') {
  ini_set('display_errors', 'On');
  ini_set('html_errors', 'On');
  error_reporting(E_ALL);
}

if ($timezone = env('APP_TIMEZONE')) {
  date_default_timezone_set($timezone);
}

$app = new Ethereal\Foundaiton\Application(
    dirname(__DIR__)
);


return $app;
